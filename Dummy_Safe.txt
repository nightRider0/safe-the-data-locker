#! python3

import *******

class Safe:
    @staticmethod
    def encrypt(hash_key, byte_inpt):
        ''' encryption goes here '''
        return byte_output

    @staticmethod
    def decrypt(hash_key, byte_inpt):
        ''' decryption goes here '''
        return byte_output

    @staticmethod
    def get_key(password):
        return '''give a hash byte'''

    # d_h is used for storing Password
    @staticmethod
    def d_h_encryption(key, value):
        '''double hash encryption input output are bytes'''

    @staticmethod
    def d_h_decryption(key, value):
        '''double hash decryption input output are bytes'''

    # d_e is used for storing Value
    @staticmethod
    def d_e_encryption(key, value):
        '''double encryption input output are bytes'''

    @staticmethod
    def d_e_decryption(key, value):
        '''double decryption input output are bytes'''

    @staticmethod
    def g_encoding(s):
        '''good encoding input output string '''

    @staticmethod
    def g_decoding(s):
         '''good decoding input output string '''

    @staticmethod
    def s_encoding(s):
        '''special encoding input output string '''

    @staticmethod
    def s_decoding(s):
        '''special decoding input output string '''
