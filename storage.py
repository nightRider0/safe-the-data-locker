#! python3
import shelve
from safe import Safe
from globalvariable import ACCESS, USERNAME
import sys


class Verification:
    @staticmethod
    def master_password(mp):
        with shelve.open(ACCESS) as db:
            if Safe.g_encoding("master") not in db.keys():
                print("Master Password not set")
                sys.exit(-4)
            if not Safe.d_h_decryption(mp.encode(), db[Safe.g_encoding('master')]) == Safe.get_key(mp.encode()):
                # master Password save key Double hash value Single hash
                print("Master Password Error ")
                sys.exit(-4)

    @staticmethod
    def password(username, value):
        with shelve.open(USERNAME) as db:
            if Safe.d_h_decryption(value.encode(), db[Safe.g_encoding(username)]) == Safe.get_key(value.encode()):
                return True
        return False

    @staticmethod
    def username(user):
        user = Safe.g_encoding(user)
        with shelve.open(USERNAME) as users:
            if user in users.keys():
                return True
        return False


class Save:
    @staticmethod
    def master_password(mp, username, password):
        try:
            with shelve.open(ACCESS) as master:
                master[Safe.g_encoding(username)] = Safe.d_e_encryption(mp.encode(), password.encode())
                return True
        except Exception as e:
            print(e.__str__() + ' in master storage')
            return False

    @staticmethod
    def user(username, password):
        try:
            with shelve.open(USERNAME) as users:
                users[Safe.g_encoding(username)] = Safe.d_h_encryption(password.encode(), password.encode())
            return True
        except Exception as e:
            print(e.__str__() + ' in user storage')
            return False
