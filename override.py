#! python3
import getpass
import os
import shelve
import sys

import pyperclip

from globalvariable import HEADER, ACCESS, USERNAME
from safe import Safe
from storage import Verification


def list_user():
    Verification.master_password(getpass.getpass("Master Password : "))
    with shelve.open(USERNAME) as user:
        print(list(Safe.g_decoding(x) for x in user.keys()))


def retrieve_password(user):
    mp = getpass.getpass("Master Password: ")
    Verification.master_password(mp)
    with shelve.open(ACCESS) as master:
        if Safe.g_encoding(user) in master.keys():
            print("echo??")
            t = Safe.d_e_decryption(mp.encode(), master[Safe.g_encoding(user)]).decode()
            if input('>>') in ['e', 'echo', 'yes', 'y']:
                print(t)
            else:
                pyperclip.copy(t)
            print("Operation Successful")
        else:
            print("User Not Exist")


def remove_account(username):
    Verification.master_password(getpass.getpass("Master Password : "))
    try:
        if Verification.username(username):
            with shelve.open(USERNAME) as user:
                user.__delitem__(Safe.g_encoding(username))
            with shelve.open(ACCESS) as master:
                master.__delitem__(Safe.g_encoding(username))
            os.remove(HEADER + Safe.s_encoding(username) + ".bak")
            os.remove(HEADER + Safe.s_encoding(username) + ".dir")
            os.remove(HEADER + Safe.s_encoding(username) + ".dat")
            print("Operation Successful ")
        else:
            print("User Not Exist")
    except Exception as e:
        print("Exception " + e.__str__())


def change_master():
    mp = getpass.getpass("Master Password : ")
    Verification.master_password(mp)
    new_mp = getpass.getpass("New Master Password : ")
    t = getpass.getpass("Confirm Master Password : ")
    if new_mp == t:
        try:
            with shelve.open(ACCESS) as master:
                master[Safe.g_encoding("master")] = Safe.d_h_encryption(new_mp.encode(), new_mp.encode())
                for key in master.keys():
                    if key == Safe.g_encoding("master"):
                        continue
                    master[key] = Safe.d_e_encryption(new_mp.encode(),
                                                  Safe.d_e_decryption(mp.encode(), master[Safe.g_encoding(key)]))
            print("Operation Successful")
        except Exception as e:
            print("Exception " + e.__str__())
    else:
        print("Password not match ")


def main():
    print("Welcome to Master Control \n Verify Yourself")
    Verification.master_password(getpass.getpass("Master Password : "))
    print("(L)ist User\n(R)etrieve Password\n(D)elete Account\n(C)hange Master")
    while True:
        choice = input("Choice >> ")
        if choice.lower() == 'l':
            list_user()
        elif choice.lower() == 'r':
            retrieve_password(input("Username>> "))
        elif choice.lower() == 'd':
            remove_account(input("Username>> "))
        elif choice.lower() == 'c':
            change_master()
        elif choice.lower() == 'e':
            print("Exit Initiated")
            sys.exit(0)
        else:
            print("Wrong Choice")

if __name__ == '__main__':
        main()
