#! python3

# successful execution 0
# wrong password -1
# wrong username -2
# unknown  reason -3
# master password error -4

import getpass
import os
import shelve
import sys

from globalvariable import HEADER, ACCESS
from password_saver import PasswordSaver
from safe import Safe
from storage import Verification, Save


def mp_entry():
    print("First time initiating Database ")
    while True:
        temp1 = getpass.getpass("Enter Master Password : ")
        temp2 = getpass.getpass("Confirm Master Password : ")

        if temp1 == temp2:
            with shelve.open(ACCESS) as master:
                master[Safe.g_encoding('master')] = Safe.d_h_encryption(temp1.encode(), temp1.encode())
            return temp1
        print("Password mismatch")


def create_directory():
    if not os.path.exists(HEADER):
        os.mkdir(HEADER)
    mp_entry()


def main():
    create_directory()
    print('''(N)ew User\n(E)xisting User\n''')
    choice = input()
    if choice.upper() == 'N':  # new User Entry
        mp = getpass.getpass("Master Password : ")
        Verification.master_password(mp)
        # Select A unique user name
        while True:
            username = input("Username : ")
            if not Verification.username(username):
                break
            print("Username " + username + " not available")
        password = getpass.getpass("Password : ")

        # save in master password
        Save.master_password(username, password, mp)
        # Save in user in Users
        Save.user(username, password)

        # create Shelve  File for user
        username = Safe.s_encoding(username)

        with shelve.open(HEADER+username):
            pass

        print("User added Successfully, Login to use")
        sys.exit(0)

    elif choice.upper() == 'E':
        username = input("Username : ")
        if not Verification.username(username):
            print("Username not exist")
            sys.exit(-1)
        password = getpass.getpass("Password : ")
        if not Verification.password(username, password):
            print("Wrong Password")
            sys.exit(-2)

        obj = PasswordSaver(username, password)

        del password

        print("(A)dd Data")
        print("(G)et Data")
        print("(C)hange Password")
        print("(D)elete Value")
        print("(K)eys in Account")
        print("A(b)out")
        print("(E)xit")

        while True:
            choice = input('Enter Your Choice : ')

            if choice.lower() == 'a':
                    # Add Value to Data base
                    temp_key = input("Key : ")
                    temp_value = input("Value : ")
                    if obj.add(temp_key, temp_value):
                        print("Operation Successful")
                    else:
                        print("Operation Failed")

            elif choice.lower() == 'g':
                    # get Value from DataBase
                    key = input("Key : ")
                    if obj.get(key):
                        print("Operation Successful")
                    else:
                        print("Operation Failed")

            elif choice.lower() == 'c':
                # Change Password
                old_pass = getpass.getpass("Old Password : ")
                new_pass = getpass.getpass("New Password : ")
                mp = getpass.getpass("Master Password : ")
                Verification.master_password(mp)
                if obj.update_password(old_password=old_pass, new_password=new_pass) and \
                        Save.master_password(mp, username, new_pass):
                    print("Operation Successful")
                else:
                    print("Operation Fail")

            elif choice.lower() == 'd':
                    # delete Value
                    temp_pass = getpass.getpass('Password : ')
                    if Verification.password(username, temp_pass):
                        key = input("Key : ")
                        if obj.delete_val(key):
                            print("Operation Successful")
                    else:
                        print("Operation Failed")

            elif choice.lower() == 'e':
                print("Exit initiated")
                sys.exit(0)

            elif choice.lower() == 'k':
                # return all keys
                password = getpass.getpass('Password : ')
                Verification.password(username, password)
                with shelve.open(HEADER+Safe.s_encoding(username)) as db:
                    print(list(Safe.g_decoding(x) for x in list(db.keys())))

            elif choice.lower() == 'b':
                    print("""this is SAFE - THE DATA LOCKER. It is a highly secure data locker, uses many advance algorithm to save your data.
                      The main motto of this locker is 'Data can destroy but cannot be theft' """)
    else:
        print("wrong choice")

'''
            elif choice.lower() == 'u':
                # Update Value
                key = input("Key : ")
                value = input("Value : ")

                if obj.update_value(key, value):
                    print("Operation Successful")
                else:
                    print("Operation Failed")
'''


if __name__ == '__main__':
    main()
