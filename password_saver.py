#! python3

import shelve
import pyperclip
from safe import Safe
from storage import Verification, Save
from globalvariable import HEADER


class PasswordSaver(Safe):
    __username = None
    __db = None
    __password = None

    def __init__(self, username, password):
        self.__username = username
        self.__password = password
        self.__db = HEADER + Safe.s_encoding(username)

    def add(self, key, value):
        key = Safe.g_encoding(key)
        with shelve.open(self.__db) as db:
            if key not in db.keys():
                    # t = Safe.d_e_encryption(self.__password.encode(), value.encode())
                    db[key] = Safe.d_e_encryption(self.__password.encode(), value.encode())
                    '''print(key)
                    print("====+=====")
                    print(t)'''
            else:
                print("value already exist")
                return False
        return True

    def get(self, key):
        key = Safe.g_encoding(key)
        # print(key)
        with shelve.open(self.__db) as db:
            # print(list(db.keys()))
            if key in list(db.keys()):
                print('echo ?')
                t = input('>>')
                if t in ['e', 'echo', 'yes', 'y']:
                    print(Safe.d_e_decryption(self.__password.encode(), db[key]).decode())
                    return True
                else:
                    pyperclip.copy(Safe.d_e_decryption(self.__password.encode(), db[key]).decode())
                    print(" Copied to Clipboard ")
                    return True
            else:
                print("You did not save this value")
        return False

    def update_password(self, old_password, new_password):
        temp = {}
        if not Verification.password(username=self.__username, value=old_password):
            print("Wrong Password")
            return False
        with shelve.open(self.__db) as db:
            keys = db.keys()
            for key in keys:
                try:
                    temp[key] = Safe.d_e_encryption(new_password.encode(),
                                                    Safe.d_e_decryption(self.__password.encode(), db[key]))
                except Exception as e:
                    print(key + e.__str__())
                    return False
            for key in keys:
                try:
                    db[key] = temp[key]
                except Exception as e:
                    print("Something gone Wrong " + e.__str__() + key)
                    return False
            Save.user(username=self.__username, password=new_password)
            self.__password = new_password

            return True

#    def update_value(self, key, value):
 #       key = Safe.g_encoding(key)
  #      with shelve.open(self.__db) as db:
   #         if key in db.keys():
    #            db[key] = Safe.d_e_encryption(self.__password.encode(), value.encode())
     #           print("Value updated")
      #          return True
       #     else:
        #        print("Key Not Exist")
         #       return False

    def delete_val(self, key):
        key = Safe.g_encoding(key)
        with shelve.open(self.__db) as db:
            if key in db.keys():
                db.__delitem__(key)
                print("Successfully deleted")
                return True
            else:
                print("Key not Exist")
                return False
